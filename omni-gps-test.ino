#include <SakuraIO.h>
#include <SoftwareSerial.h>
#include <TinyGPS++.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS_1 2

SakuraIO_I2C sakuraio;

// GPS
const uint8_t rxPin = 10;
const uint8_t txPin = 11;
SoftwareSerial ss(rxPin, txPin);

TinyGPSPlus gps;

//const unsigned long GPS_UPDATE_INTERVAL = 60000 * 30;
const unsigned long GPS_UPDATE_INTERVAL = 1000;

String mCommand = "";
boolean bIsValidCommand = false;

uint8_t result = 0;

const uint32_t gpsBaudRate = 9600;
const unsigned int GPS_BUFFER_SIZE = 100;
char rawGPSInfo[GPS_BUFFER_SIZE] = "";
int gpsInfoCursor = 0;
uint32_t numSatellites = 0;;
float lat = 0.f;
float lng = 0.f;


// temperature
OneWire oneWire(ONE_WIRE_BUS_1);

DallasTemperature temperatureSensors(&oneWire);

DeviceAddress temperatureShort = { 0x28, 0xA8, 0x18, 0xE4, 0x8, 0x0, 0x0, 0xC3 };
DeviceAddress temperatureLong = { 0x28, 0x39, 0x44, 0x4E, 0x8, 0x0, 0x0, 0x43 };
DeviceAddress temperatureInner = { 0x28, 0x93, 0x4F, 0x6E, 0x8, 0x0, 0x0, 0xD0 };

float temperatureValueShort = 0.f;
float temperatureValueLong = 0.f;
float temperatureValueInner = 0.f;

unsigned long queuedTime = 0;

bool bIsECUpdated = false;
bool bIsGPSUpdated = false;
bool bNeedToSendData = false;

//---------------------------------------------------
void initTemperatureSensors()
{
	temperatureSensors.begin();
	temperatureSensors.setWaitForConversion(true);
}

//---------------------------------------------------
void updateGPSInfo()
{
	if (gps.satellites.isValid())
	{
		numSatellites = gps.satellites.value();
	}

	if (gps.location.isValid())
	{
		lat = gps.location.lat();
		lng = gps.location.lng();

		bIsGPSUpdated = true;
	}
	else
	{
		bIsGPSUpdated = false;
	}
}

//---------------------------------------------------
void updateTemperature()
{
	temperatureSensors.requestTemperaturesByAddress(temperatureShort);
	temperatureSensors.requestTemperaturesByAddress(temperatureLong);
	temperatureSensors.requestTemperaturesByAddress(temperatureInner);

	temperatureValueShort = temperatureSensors.getTempC(temperatureShort);
	temperatureValueLong = temperatureSensors.getTempC(temperatureLong);
	temperatureValueInner = temperatureSensors.getTempC(temperatureInner);
}

//---------------------------------------------------
void setup()
{
	Serial.begin(115200);
	ss.begin(gpsBaudRate);

	Serial.print("Trying to establish connection of sakura module.");

	for (;;)
	{
		if ((sakuraio.getConnectionStatus() & 0x80) == 0x80)
		{
			Serial.println("");
			Serial.println("Connected!");
			break;
		}

		Serial.print(".");
		delay(1000);
	}

	Serial.println("");

	initTemperatureSensors();
}

//---------------------------------------------------
void loop()
{
	while (ss.available() > 0)
	{
		if (gps.encode(ss.read()))
		{
			updateGPSInfo();
		}
	}

	

	if (0 == queuedTime || (millis() - queuedTime) > GPS_UPDATE_INTERVAL)
	{
		if (bIsGPSUpdated)
		{
			enqueueGPS();
			printGPSInfo();
		}

		updateTemperature();
		printTemperatures();

		//enqueueTemperature();

		//sakuraio.send();

		queuedTime = millis();
	}
}

//---------------------------------------------------
void enqueueTemperature()
{
	sakuraio.enqueueTx((uint8_t)4, temperatureValueShort);
	sakuraio.enqueueTx((uint8_t)5, temperatureValueLong);
	sakuraio.enqueueTx((uint8_t)6, temperatureValueInner);
}

//---------------------------------------------------
void enqueueGPS()
{
	sakuraio.enqueueTx((uint8_t)0, numSatellites);
	sakuraio.enqueueTx((uint8_t)1, lat);
	sakuraio.enqueueTx((uint8_t)2, lng);
}

//---------------------------------------------------
void printGPSInfo()
{
	Serial.println("GPS -----------");
	Serial.print("satellites: ");
	Serial.print(numSatellites);
	Serial.print(", lat: ");
	Serial.print(lat, 6);
	Serial.print(", long: ");
	Serial.print(lng, 6);
	Serial.println("\n");
}

//---------------------------------------------------
void printTemperatures()
{
	Serial.println("Temperatures -----------");
	Serial.print("1m: ");
	Serial.print(temperatureValueShort, 6);
	Serial.print(", 5m: ");
	Serial.print(temperatureValueLong, 6);
	Serial.print(", inner: ");
	Serial.print(temperatureValueInner, 6);
	Serial.println("\n");
}
